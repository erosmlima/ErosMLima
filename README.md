<!-- Cover -->
<p align="center">
	<img 
		alt=" " 
		loading="lazy"
		src="./assets/Cover_GitHub.gif" 
		style="border-radius: 15px; box-shadow: 5px 5px 5px 5px rgba(0,0,0,.5);" 
		title="Eros MLima" 
	>
</p>

<h1>⚡️ErosMLima☕️</h1> 

## Hello, How are you? 😀🗺️

👨 My name is **Eros MLima** (future Agile Scrum Master³), I have 35 years, I'm focused in **Front-end / Back-end / Mobile / Cybersecurity** for now I'm a generalist developer.🎓

📚 I'm studying [Computer Sciences¹ and Software Engineering²](https://www.igti.com.br/custom/desenvolvedor-front-end/) through a Bootcamp offered by *Digital Innovation One Inc. and Harvard CS50* - making courses and lectures for achieve my goals. I'm developing applications using the tecnologies like: 
**Python**, **JavaScript**, **PHP**, **.NET**, **NodeJS**, **React**, **C#**, **NoSQL**, **Django**, **Flask** and **MongoDB**.

## Skills☕️ 

<!-- Skills -->
Some of my skills and knowledges like Jr. Developer :
- 👔 **Front-end**: HTML, CSS, JavaScript (Ajax, jQuery, ES6+), Bootstrap, Angular 4+, React...
- 💻 **Back-end**: Django, Flask, NodeJS, ExpressJS, AdonisJS, Laravel, CodIgniter, Zend...
- 📲 **Mobile**: React Native, Swift, Objective-C, Xcode, Xamarin, Thunkable, Kotlin...
- 🔠 **Languages**: JavaScript, Python, PHP, .NET, C#, Java...
- 🏷️ **Versioning**: Git, GitHub and Bitbucket.
- 🌦️ **Cloud**:  Mining [Kryptos], Docker, Kubernets, AWS, Azure, Security...
- 🧪 **Tests and Automatization**: NPM, Jest and Gulp.
- 🔰 **Databases**: Language SQL, NoSQL, MongoDB, SQLite, OracleDB and MySQL.
- 🏗️ **Infrastructure**: HTTP, TCP/IP, Windows 10, Kali and MacOs.
- ⚜️ **Architecture**: Rest, MVC, APIS, OOP, S.O.L.I.D. and whatever I need.
- 📈 **Software Egineering**: Requirements gathering, UML, Agile, Scrum and Kanban.

<!-- Contacts -->

**Contacts: ★★★★★**

- 💬 **Reach me through** website4creators@gmail.com or
- 📲 ** Linkedin, Zoom, Skype, Messenger, Gmail or Telegram in a friendly meeting chat, any time for☕️.
<!-- Charts -->
<img src="https://github-readme-stats.vercel.app/api/top-langs/?username=ErosMLima&layout=compact&theme=jolly"
style="max-width:120%" align="center">


<!-- Languages, libs and frameworks -->
<p align="left">
	<img alt="HTML5" src="https://img.shields.io/badge/-HTML-fff?style=plastic&logo=HTML5" title="HTML5" />
	<img alt="CSS3" src="https://img.shields.io/badge/-CSS-fff?style=plastic&logo=CSS3&logoColor=1572B6" title="CSS3" />
	<img alt="JavaScript" src="https://img.shields.io/badge/-JavaScript-fff?fff&style=plastic&logo=javascript&logoColor=f7ab00" title="JavaScript" />
	<img alt="jQuery" src="https://img.shields.io/badge/-jQuery-fff?style=plastic&logo=jquery&logoColor=4878a0" title="jQuery" />
	<img alt="JSON" src="https://img.shields.io/badge/-JSON-fff?style=plastic&logo=json&logoColor=1a1a1a" title="JSON" />
	<img alt="TypeScript" src="https://img.shields.io/badge/-TypeScript-fff?style=plastic&logo=typescript" title="TypeScript" />
	<img alt="MySQL" src="https://img.shields.io/badge/-MySQL-fff?style=plastic&logoColor=00758f&logo=mysql" title="MySQL" />
	<img alt="MongoDB" src="https://img.shields.io/badge/-MongoDB-fff?style=plastic&logoColor=009547&logo=mongodb" title="MongoDB" />
	<img alt="Node.js" src="https://img.shields.io/badge/-Node.js-fff?style=plastic&logoColor=fff&logo=node.js&logoColor=5B9856" title="Node.js" />
	<img alt="Bootstrap" src="https://img.shields.io/badge/-Bootstrap-fff?style=plastic&logo=bootstrap&logoColor=563D7C" title="Bootstrap" />
	<img alt="Angular.js" src="https://img.shields.io/badge/-Angular-fff?style=plastic&logo=angular&logoColor=af2d2f" title="Angular.js" />
	<img alt="ReactJS" src="https://img.shields.io/badge/-React-fff?style=plastic&logo=react&logoColor=18BCEE" title="ReactJS" />
</p>

<!-- Tools Front-end -->
<p align="left">
	<img alt="Babel" src="https://img.shields.io/badge/-Babel-fff?style=plastic&logo=babel" title="Babel" />
	<img alt="Figma" src="https://img.shields.io/badge/-Figma-fff?fff&style=plastic&logo=figma" title="Figma" />
	<img alt="Git" src="https://img.shields.io/badge/-Git-fff?style=plastic&logo=git" title="Git" />
	<img alt="GitHub" src="https://img.shields.io/badge/-GitHub-fff?style=plastic&logo=github&logoColor=333333" title="GitHub" />
	<img alt="Gulp" src="https://img.shields.io/badge/-Gulp-fff?style=plastic&logo=gulp" title="Gulp" />
	<img alt="Jest" src="https://img.shields.io/badge/-Jest-fff?style=plastic&logo=jest&logoColor=944058" title="Jest" />
	<img alt="NPM" src="https://img.shields.io/badge/-NPM-fff?style=plastic&logo=npm" title="NPM" />
	<img alt="Visual Studio Code" src="https://img.shields.io/badge/-Visual%20Studio%20Code-fff?style=plastic&logo=visual-studio-code&logoColor=007ACC" title="Visual Studio Code" />
	<img alt="Webpack" src="https://img.shields.io/badge/-Webpack-fff?style=plastic&logo=webpack&logoColor=1b74ba" title="Webpack" />
</p>

#GoGoGo 🚀 🚀 🚀
